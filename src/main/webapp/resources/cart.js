var lastCartUpdate = 0;
var req;

function updateAdminMainPage() {
    var url = "/admin/main";
    window.location.href = url;
}

function addProduct() {
    var url = "/admin/add";
    window.location.href = url;
}

function deleteProductMain() {
    var url = "/admin/listProductDelete/pizza";
    window.location.href = url;
}
function editProductMain() {
    var url = "/admin/listProductEdit/pizza";
    window.location.href = url;
}

function viewOrder() {
    var url = "/admin/orderNotAccepts/notAccepted";
    window.location.href = url;
}

function deleteOrderAdmin(id, typeList) {
    var url = "/admin/deleteOrderAdmin/" + id + "/" + typeList;
    window.location.href = url;
}

function showOrderMore(id) {
    req = newXMLHttpRequest();
    var url = "/admin/getInfo/" + id;
    req.onreadystatechange = getReadyStateHandler(req, updateInfo);
    req.open("Get", url, true);
    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    req.send(null);
}
function updateInfo(str) {
    var inner = document.getElementById('info');
    inner.innerHTML = str;
    $('#hide-layout, #info').fadeIn(300);
}
function changeOrderStatus(typeList, orderId) {
    var selectBox = document.getElementById("choiceOrderStatus");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    var url = "/admin/orderChangeAccepts/" + selectedValue + "/" + typeList + "/" + orderId;
    window.location.href = url
}

function changeFunc() {
    var selectBox = document.getElementById("choice-category");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    var url = "/listProduct/" + selectedValue;
    window.location.href = url
}
function changeFuncEdit() {
    var selectBox = document.getElementById("choice-category");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    var url = "/admin/listProductEdit/" + selectedValue;
    window.location.href = url;
}

function editProduct(id) {
    var url = "/admin/editProduct/" + id;
    window.location.href = url;
}

function saveEditProduct(type) {
    var url = "/admin/saveModifiedProduct/" + type;
    window.location.href = url;
}

function changeFuncDelete() {
    var selectBox = document.getElementById("choice-category");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    var url = "/admin/listProductDelete/" + selectedValue;
    window.location.href = url;
}

//переход на форму оформления заказа
function makeOrder() {
    var url = "/order";
    window.location.href = url
}

function makeHello() {
    var url = "/hello";
    window.location.href = url
}

//updating of the basket when the page loads
function updatePage() {
    req = newXMLHttpRequest();
    var url = "/updateCartMethod";
    req.onreadystatechange = getReadyStateHandler(req, updateCart);
    req.open("Get", url, true);
    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    req.send(null);
}

/*
 * Adds the specified item to the shopping cart, via Ajax call
 * itemCode - product code of the item to add
 */
function addToCart(id) {

    var url = "/listItemCart/" + id;
    req.onreadystatechange = getReadyStateHandler(req, updateCart);
    req.open("Get", url, true);
    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    req.send(null);
}

function deleteProduct(id, name) {
    var url = "/admin/deleteProduct/" + id + "/" + name;
    window.location.href = url;
}

/*
 * Update shopping-cart area of page to reflect contents of cart.
 */
function updateCart(str) {
    var inner = document.getElementById('cart_div');
    inner.innerHTML = str;
    $("#cart_div #orderMake").click(makeOrder);
}

//удаление продукта из корзины
function deleteItem(id) {
    req = newXMLHttpRequest();
    var url = "/delete/" + id;
    req.onreadystatechange = getReadyStateHandler(req, updateCart);
    req.open("Get", url, true);
    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    req.send(null);
}
function saveOrder() {
    var url = "/saveOrder";
    window.location.href = url
}