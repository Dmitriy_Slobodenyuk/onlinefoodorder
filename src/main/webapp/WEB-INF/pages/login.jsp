<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/style-login.css" />"/>
    <title>Регистрация</title>
</head>
<body>
<section class="container">
    <div class="login">
        <h1>Вход в систему</h1>

        <form method="post" action="<c:url value="/j_spring_security_check"/>">
            <p><input type="text" name="j_username" value="" placeholder="Имя пользователя"></p>

            <p><input type="password" name="j_password" value="" placeholder="Пароль"></p>

            <p class="remember_me">
                <label>
                    <input type="checkbox" name="_spring_security_remember_me" id="remember_me">
                    Запомнить меня
                </label>
            </p>

            <p class="submit"><input type="submit" name="registration_submit" value="Войти"></p>
        </form>
        <c:if test="${not empty error}">
            <font color="red">
                    ${error} </font>
        </c:if>
    </div>
</section>

<section class="about">
    <p class="about-links">
        <a href="${pageContext.request.contextPath}/hello" target="_parent">Вернуться на главную</a>
        <a href="" target="_parent">Правила сайта</a>
    </p>

    <p class="about-author">Вход пока только для админа</p>

    <p>Реализация авторизации пользователей на сайте , на стадии разработки !</p>
</section>
</body>
</html>