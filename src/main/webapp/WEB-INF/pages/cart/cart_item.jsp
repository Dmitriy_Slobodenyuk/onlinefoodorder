<%@page import="java.util.*" %>
<%@ page import="com.onlinefoodorder.cart.Cart" %>
<%@ page import="com.onlinefoodorder.entity.Product" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<table id="part" class="li-item">
    <c:forEach items="${cartList}" var="entry">
        <tr>
            <td class="nametd">${entry.key.getName()}</td>
            <td class="tdprice">${entry.key.getPrice()} грн.</td>
            <td class="tdcount">${entry.value} шт.</td>
            <td>
                <%--удаление продукта из корзины--%>
                <button class="buttClose" type="button" onclick="deleteItem('${entry.key.getId()}')">
                </button>
            </td>
        </tr>
    </c:forEach>
</table>
<span id="summ"><span id="total">Cумма : ${totalPrice} грн.</span></span>

<button id="orderMake" class="buttOrder" type="button" onclick="makeOrder()"><span>Оформить</span>
</button>
