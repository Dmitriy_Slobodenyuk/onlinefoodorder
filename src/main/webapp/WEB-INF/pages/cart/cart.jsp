<%@page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/contents.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/templatemo_style.css" />"/>
    <title>Web cafe</title>
    <link rel="shortcut icon" href="/resources/images/main/next.png" type="image/x-icon">
</head>
<body>
<div id="hello_site_title_bar_wrapper">

    <%@ include file="../include/cap.jsp" %>
    <div id="rest-holder">
        <div id="list_block">
            <a id="header_block">Корзина</a>
            <ul>
                <c:forEach items="${listOrder}" var="product">
                    <li id="rest-block">
                        <a id="rest-logo" style="background-image: url('${product.getImage()}')"></a>
                        <a id="name_product">${product.getName()}</a>
                        <a id="desc_product">${product.getDescription()}</a>
                        <a id="weight_product">${product.getWeight()} г.</a>
                        <a id="price_product">${product.getPrice()} грн.</a>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
    <div class="button_01"><a href="#">Вверх</a></div>
    <%@ include file="../include/bottom.jsp" %>
    <div class="cleaner"></div>
</div>
</body>
</html>
