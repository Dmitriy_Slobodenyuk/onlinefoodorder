<%@page import="java.util.*" %>
<%@ page import="com.onlinefoodorder.cart.Cart" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/contents.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/style.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/templatemo_style.css" />"/>
    <script type="text/javascript" src="<c:url value="/resources/ajax.js"/> "></script>
    <script type="text/javascript" src="<c:url value="/resources/cart.js" /> "></script>
    <script type="text/javascript" src="<c:url value="/resources/jquery-2.1.1.min.js" /> "></script>
    <title>Web cafe</title>
    <link rel="shortcut icon" href="/resources/images/main/next.png" type="image/x-icon">
</head>
<body>
<div id="error-find" style="background-image: url('/resources/images/netu.jpg')">
</div>
</body>
</html>
