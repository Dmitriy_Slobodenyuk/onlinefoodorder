<%@page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/styles/templatemo_style.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/contents.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/admin-css.css" />"/>
    <script type="text/javascript" src="<c:url value="/resources/ajax.js"/> "></script>
    <script type="text/javascript" src="<c:url value="/resources/cart.js" /> "></script>
    <script type="text/javascript" src="<c:url value="/resources/jquery.js"/> "></script>
    <script type="text/javascript" src="<c:url value="/resources/orderjs.js"/> "></script>
    <title>
        Web cafe</title>
    <link rel="shortcut icon" href="img/main/next.png" type="image/x-icon">
</head>
<body>
<%@ include file="../include/cap.jsp" %>

<div id="main_admin">
    <div id="console-head"><p>Админ консоль</p></div>

    <div id="product-edit">
        <div class="add" onclick="addProduct()">
            <br>
            <br>
            <a>Добавить продукт</a>
        </div>
        <div class="add" onclick="deleteProductMain()">
            <br>
            <br>
            <a>Удалить продукт</a>
        </div>
        <div class="add" onclick="editProductMain()">
            <br>
            <br>
            <a>Редактировать</a>

        </div>
        <div class="add" onclick="viewOrder()">
            <br>
            <br>
            <a>Заказы</a>
            <a class="count-order">- ${count}</a>
        </div>

    </div>
</div>
<%@ include file="../include/bottom.jsp" %>
<div class="cleaner"></div>
</body>
</html>
