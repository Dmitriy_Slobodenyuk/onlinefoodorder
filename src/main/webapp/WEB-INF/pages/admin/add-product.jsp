<%@page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/styles/templatemo_style.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/contents.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/admin-css.css" />"/>
    <title>
        Web cafe</title>
    <link rel="shortcut icon" href="img/main/next.png" type="image/x-icon">
</head>
<body>
<%@ include file="../include/cap.jsp" %>
<div id="back">
    <a href="${pageContext.request.contextPath}/admin/main">Назад</a><span>${state}</span>
</div>

<form id="add-form" method="post" enctype="multipart/form-data"
      action="${pageContext.request.contextPath}/admin/saveProduct">

    <label>ДОБАВЛЕНИЕ ПРОДУКТА</label><br/><br/>
    <label>Название продукта</label><br/>
    <span>* </span><input name="productName" type="text"></br></br>
    <label>Категория</label><br/>
    <select name="type">
        <option value="Пицца">Пицца</option>
        <option value="Суши">Суши</option>
        <option value="фастфуд">Фастфуд</option>
        <option value="Мясное блюдо">Мясное блюдо</option>
        <option value="Домашнее меню">Домашнее меню</option>
        <option value="Выпечка">Выпечка</option>
        <option value="Напитки">Напитки</option>
        <option value="Бизнес-ланч">Бизнес-ланч</option>
    </select><br/><br/>

    <label>Вес</label><br/>
    <span>* </span><input name="weight" type="text"><br/></br>


    <label>Цена</label><br/>
    <span>* </span><input name="price" type="text"><br/></br>

    <label>Фото</label><br/>
    <input id="photo" name="photoFile" type="file"><br/></br>

    <label>Описание</label><br/>
    <textarea name="description"></textarea><br/></br>
    <button type="submit"><span>Добавить</span></button>

</form>
<%@ include file="../include/bottom.jsp" %>
<div class="cleaner"></div>
<div class="cleaner"></div>
</body>
</html>
