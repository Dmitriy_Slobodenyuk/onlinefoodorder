<%@page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/styles/templatemo_style.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/contents.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/admin-css.css" />"/>
    <title>
        Web cafe</title>
    <link rel="shortcut icon" href="img/main/next.png" type="image/x-icon">
</head>
<body>
<%@ include file="../include/cap.jsp" %>
<div id="back">
    <a href="${pageContext.request.contextPath}/admin/main">Назад</a><span>${state}</span>
</div>
<form id="add-form" method="post" enctype="multipart/form-data"
      action="${pageContext.request.contextPath}/admin/saveModifiedProduct">

    <label>Редактировать продукт</label><br/><br/>

    <label>id продукта</label><br/>
    <span>* </span><input name="productId" type="text" value="${id}"></br></br>

    <label>Название продукта</label><br/>
    <span>* </span><input name="productName" type="text" value="${name}"></br></br>

    <label>Категория</label><br/>
    <select name="type">
        <option ${type == 'Пицца' ? 'selected' : ''} value="Пицца">Пицца</option>
        <option ${type == 'Суши' ? 'selected' : ''} value="Суши">Суши</option>
        <option ${type == 'Фастфуд' ? 'selected' : ''} value="Фастфуд">Фастфуд</option>
        <option ${type == 'Мясное блюдо' ? 'selected' : ''} value="Мясное блюдо">Мясное блюдо</option>
        <option ${type == 'Домашнее меню' ? 'selected' : ''} value="Домашнее меню">Домашнее меню</option>
        <option ${type == 'Выпечка' ? 'selected' : ''} value="Выпечка">Выпечка</option>
        <option ${type == 'Напитки' ? 'selected' : ''} value="Напитки">Напитки</option>
        <option ${type == 'Бизнес-Ланч' ? 'selected' : ''} value="Бизнес-Ланч">Бизнес-ланчи</option>
    </select><br/><br/>

    <label>Вес</label><br/>
    <span>* </span><input name="weight" type="text" value="${weight}"><br/></br>

    <label>Цена</label><br/>
    <span>* </span><input name="price" type="text" value="${price}"><br/></br>

    <label>Фото</label><br/>
    <input id="photo" name="photo" type="text" value="${photo}"><br/></br>
    <input id="photo" name="photoFile" type="file"><br/></br>

    <label>Описание</label><br/>

    <textarea name="description">${description}</textarea><br/></br>
    <button type="submit"><span>Сохранить</span></button>

</form>


<%@ include file="../include/bottom.jsp" %>
<div class="cleaner"></div>
<div class="cleaner"></div>
</body>
</html>
