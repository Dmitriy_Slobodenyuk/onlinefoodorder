<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/templatemo_style.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/order-styles.css" />"/>
    <script type="text/javascript" src="<c:url value="/resources/ajax.js"/> "></script>
    <script type="text/javascript" src="<c:url value="/resources/cart.js" /> "></script>
    <script type="text/javascript" src="<c:url value="/resources/jquery-2.1.1.min.js" /> "></script>
    <title>Web cafe</title>
    <link rel="shortcut icon" href="/resources/images/main/next.png" type="image/x-icon">

</head>
<body>
<%@ include file="../include/cap.jsp" %>
<div id="main_title">
    <span>ОФОРМЛЕНИЕ ЗАКАЗА</span>
</div>

<form:form id="main_form" method="POST" commandName="clientInfo" action="${pageContext.request.contextPath}/saveOrder">
    <table>
        <tr>
            <td><form:input path="name" placeholder=" Имя (Обяз)"/></td>
            <td><form:errors path="name" cssClass="error"/></td>
        </tr>
        <tr>
            <td><form:input path="phone" placeholder=" Телефон (Обяз)"/></td>
            <td><form:errors path="phone" cssClass="error"/></td>
        </tr>
        <tr>
            <td><form:input path="mail" placeholder=" E-mail (Обяз)"/></td>
            <td><form:errors path="mail" cssClass="error"/></td>
        </tr>
        <tr>
            <td><form:input path="street" placeholder=" Улица (Обяз)"/></td>
            <td><form:errors path="street" cssClass="error"/></td>
        </tr>
        <tr>
            <td><form:input path="homeNumber" placeholder=" Номер дома (Обяз)"/></td>
            <td><form:errors path="homeNumber" cssClass="error"/></td>
        </tr>
        <tr>
            <td><form:input path="officeNumber" placeholder=" Номер кв / оф (Обяз)"/></td>
            <td><form:errors path="officeNumber" cssClass="error"/></td>
        </tr>
        <tr>
            <td><textarea name="comment" placeholder="Комментарий к заказу..."></textarea></td>
            <td>
                <ul class="pay-comm">
                    <li><span>Форма оплаты</span></li>
                    <li>
                        <select id="choice-category" style="text-align: left">
                            <option selected value="pizza">Наличные</option>
                            <option value="sushi">Visa/MasterCard</option>
                            <option value="fastFood">Приват24</option>
                            <option value="meat">WebMoney</option>
                        </select>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><label>К оплате : ${total} грн.</label></td>
        </tr>
        <tr>
            <td colspan="1"><input class="buttOrder" type="submit" value="Заказать"/></td>
            <td><label id="sms">${message}</label></td>
        </tr>
    </table>
</form:form>
<%@ include file="../include/bottom.jsp" %>
<div class="cleaner"></div>
</body>
</html>
