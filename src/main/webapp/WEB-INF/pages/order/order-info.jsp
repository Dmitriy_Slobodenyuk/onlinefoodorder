<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<table id="tbl-info-main">
    <tr style="background-color: #DEE5EB">
        <td>Информация о клиенте</td>
        <td>Заказ <a href="">Передать на кухню..</a></td>
    </tr>
    <tr>
        <td>
            <p>Id заказа: <span>${idOrder}</span></p><br>
            <p>Имя: <span>${name}</span></p>
            <p>Телефон: <span>${phone}</span></p>
            <p>E-mail: <span>${mail}</span></p>
            <p>Адрес: <span>${adress}</span></p>
            <p>Сумма: <span>${price} грн.</span></p>
            <p>Комент-ий: <span>${comment}</span></p>
        </td>
        <td>
            <table id="table-info">
                <c:forEach items="${map}" var="entry">
                    <tr>
                        <td class="name-pr">${entry.key.name}</td>
                        <td class="weight-pr">${entry.key.weight} г.</td>
                        <td class="price-pr">${entry.key.price} грн.</td>
                        <td class="count-pr">${entry.value} шт.</td>
                    </tr>
                </c:forEach>
            </table>
        </td>
    </tr>
</table>

<button id="btn-close" onclick="closeInfo()"></button>

