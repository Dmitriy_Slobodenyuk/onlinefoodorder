<%@page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/order.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/templatemo_style.css" />"/>
    <script type="text/javascript" src="<c:url value="/resources/ajax.js"/> "></script>
    <script type="text/javascript" src="<c:url value="/resources/cart.js" /> "></script>
    <script type="text/javascript" src="<c:url value="/resources/jquery.js"/> "></script>
    <script type="text/javascript" src="<c:url value="/resources/orderjs.js"/> "></script>
    <title>Web cafe</title>
    <link rel="shortcut icon" href="/resources/images/main/next.png" type="image/x-icon">
</head>
<body>
<%@ include file="../include/cap.jsp" %>
<div id="head">
    <h2>Список заказов</h2>
</div>
<div id="div-ref">
    <a href="${pageContext.request.contextPath}/admin/main">Назад</a>
    <a href="${pageContext.request.contextPath}/admin/orderNotAccepts/notAccepted">Не принятые</a>
    <a href="${pageContext.request.contextPath}/admin/orderNotAccepts/accepted">Принятые</a>
    <a href="${pageContext.request.contextPath}/admin/orderNotAccepts/delivered">Доставленные</a>
</div>

<div id="div-order">
    <table id="table-order">
        <tr style="background-color: #DEE5EB">
            <td>№ Заказа</td>
            <td>Время заказа</td>
            <td>Цена</td>
            <td>Статус</td>
            <td>Инф.</td>
            <td>Ред.</td>
        </tr>
        <c:forEach items="${orderList}" var="order">
            <tr>
                <td>${order.id}</td>
                <td>${order.dateOrder}</td>
                <td>${order.price} грн.</td>
                <td><select id="choiceOrderStatus" style="text-align: left"
                            onchange="changeOrderStatus('${typeList}','${order.id}')">
                    <option ${typeList == 'notAccepted' ? 'selected' : ''} value="notAccepted">Не принят</option>
                    <option ${typeList == 'accepted' ? 'selected' : ''} value="accepted">Принят</option>
                    <option ${typeList == 'delivered' ? 'selected' : ''} value="delivered">Доставлен</option>
                </select></td>
                <td>
                    <button id="btn_more" type="button" onclick="showOrderMore('${order.id}')">
                        <span class="btn_text">показать...</span>
                    </button>
                </td>
                <td>
                        <%--<a href="${pageContext.request.contextPath}/admin/deleteOrderAdmin/${order.id}/${typeList}">удалить...</a>--%>
                    <button id="btn_delete" type="button" onclick="deleteOrderAdmin('${order.id}','${typeList}')">
                        <span class="btn_text">удалить...</span>
                    </button>
                </td>
            </tr>

        </c:forEach>
    </table>
    <div id="info"></div>
</div>
<div class="button_01"><a href="#">Вверх</a></div>

<%@ include file="../include/bottom.jsp" %>
<div class="cleaner"></div>
<div id="hide-layout" class="hide-layout"></div>
</body>
</html>
