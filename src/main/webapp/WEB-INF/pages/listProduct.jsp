<%@page import="java.util.*" %>
<%@ page import="com.onlinefoodorder.cart.Cart" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/contents.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/style.css" />"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/templatemo_style.css" />"/>

    <script type="text/javascript" src="<c:url value="/resources/ajax.js"/> "></script>
    <script type="text/javascript" src="<c:url value="/resources/cart.js" /> "></script>
    <script type="text/javascript" src="<c:url value="/resources/jquery-2.1.1.min.js" /> "></script>
    <title>Web cafe</title>
    <link rel="shortcut icon" href="/resources/images/main/next.png" type="image/x-icon">
</head>
<body>


<%@ include file="include/cap.jsp" %>

<div id="rest-holder">

    <div id="sidebar">
        <div class="boxed">
            <h2 class="title">Корзина
                <i id="icon_cart"></i>
            </h2>

            <div id="cart_div">
            </div>
        </div>
    </div>
    <div id="list_block">
        <div id="header_block">
            <select id="choice-category" style="text-align: left" onchange="changeFunc()">
                <option ${name == 'Пицца' ? 'selected' : ''} value="pizza">Пицца</option>
                <option ${name == 'Суши' ? 'selected' : ''} value="sushi">Суши</option>
                <option ${name == 'фастфуд' ? 'selected' : ''} value="fastFood">Фастфуд</option>
                <option ${name == 'Мясное блюдо' ? 'selected' : ''} value="meat">Мясное блюдо</option>
                <option ${name == 'Домашнее меню' ? 'selected' : ''} value="homeMenu">Домашнее меню</option>
                <option ${name == 'Выпечка' ? 'selected' : ''} value="baking">Выпечка</option>
                <option ${name == 'Напитки' ? 'selected' : ''} value="drink">Напитки</option>
                <option ${name == 'Бизнес-Ланч' ? 'selected' : ''} value="launch">Бизнес-ланчи</option>
            </select>

        </div>
        <ul class="list-product">
            <c:forEach items="${listProduct}" var="product">
                <li id="rest-block">
                    <a id="rest-logo" style="background-image: url('${product.image}')"></a>
                    <a id="name_product">${product.name}</a>
                    <a id="desc_product">${product.description}</a>
                    <a id="weight_product">${product.weight} г.</a>
                    <a id="price_product">${product.price} грн.</a>
                    <a>
                        <button id="btn_cart" type="button" onclick="addToCart('${product.id}')">
                            <span id="btn_txt">В Корзину</span>
                        </button>
                    </a>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>

<div class="button_01"><a href="#">Вверх</a></div>

<%@ include file="include/bottom.jsp" %>
<div class="cleaner"></div>

<script>
    updatePage();
</script>
</body>
</html>
