<%@page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="/resources/styles/templatemo_style.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/contents.css" />"/>
    <title>
        Web cafe</title>
    <link rel="shortcut icon" href="img/main/next.png" type="image/x-icon">
</head>

<body>
<%@ include file="include/cap.jsp" %>
<div id="hello_content">
    <h2>
        <a class="content_like" target="_blank">КАТЕГОРИИ</a>
    </h2>

    <div class="product_box margin_r_10">
        <a href="${pageContext.request.contextPath}/listProduct/pizza"><img src="img/main/pizza-index.jpg"
                                                                            alt="image"/></a>

        <h3>ПИЦЦА</h3>
    </div>
    <div class="product_box margin_r_10">
        <a href="${pageContext.request.contextPath}/listProduct/sushi"><img src="img/main/sushi-index.jpg"
                                                                            alt="image"/></a>

        <h3>СУШИ</h3>
    </div>
    <div class="product_box margin_r_10">
        <a href="${pageContext.request.contextPath}/listProduct/fastFood"><img src="img/main/fast-index.jpg"
                                                                               alt="image"/></a>

        <h3>ФАСТФУД</h3>
    </div>
    <div class="product_box">
        <a href="${pageContext.request.contextPath}/listProduct/meat"><img src="img/main/steak.png"
                                                                           alt="image"/></a>

        <h3>МЯСНОЕ БЛЮДО</h3>
    </div>
    <div class="cleaner"></div>
    <div class="product_box margin_r_10">
        <a href="${pageContext.request.contextPath}/listProduct/homeMenu"><img src="img/main/kitchen.png"
                                                                               alt="image"/></a>

        <h3>ДОМАШНЕЕ МЕНЮ</h3>
    </div>
    <div class="product_box margin_r_10">
        <a href="${pageContext.request.contextPath}/listProduct/baking"><img src="img/main/baking-index.jpg"
                                                                             alt="image"/></a>

        <h3>ВЫПЕЧКА</h3>
    </div>
    <div class="product_box margin_r_10">
        <a href="${pageContext.request.contextPath}/listProduct/drink"><img src="img/main/drinks-index.jpg"
                                                                            alt="image"/></a>

        <h3>НАПИТКИ</h3>
    </div>
    <div class="product_box">
        <a href="${pageContext.request.contextPath}/listProduct/launch"><img src="img/main/lanch.png"
                                                                             alt="image"/></a>

        <h3>БИЗНЕС-ЛАНЧИ</h3>
    </div>
    <div class="cleaner_h20"></div>
    <div class="button_01"><a href="#">Вверх</a></div>
    <div class="cleaner"></div>
</div>

<%@ include file="include/bottom.jsp" %>
<div class="cleaner"></div>
</body>
</html>