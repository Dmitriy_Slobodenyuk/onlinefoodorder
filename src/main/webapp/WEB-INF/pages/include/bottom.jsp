<%@page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
<div id="templatemo_content_bottom"></div>

<div id="templatemo_footer_wrapper">
    <div id="templatemo_footer">

        <div class="section_w220">

            <h4>Партнеры</h4>

            <ul class="footer_list_menu">
                <li><a href="" rel="nofollow" target="_parent">Партнер 1</a></li>
                <li><a href="" target="_parent">Партнер 2</a></li>
                <li><a rel="nofollow" href="" target="_parent">Партнер 3</a></li>
            </ul>

        </div>

        <div class="section_w220">

            <h4>Больше</h4>

            <ul class="footer_list_menu">
                <li><a href="#">Как заказать</a></li>
                <li><a href="#">Бонусная система</a></li>
                <li><a href="#">Соглашение</a></li>
            </ul>

        </div>

        <div class="section_w220">

            <h4>Ещё</h4>

            <ul class="footer_list_menu">
                <li><a href="#">Корпоративным клиентам</a></li>
                <li><a href="#">Сотрудничество</a></li>
                <li><a href="#">Карты</a></li>
            </ul>

        </div>


        <div class="section_w220">

            <h4>О нас</h4>

            <p>Дополнительная информация о сайте...
            </p>

        </div>

        <div class="cleaner"></div>

    </div>
</div>
</body>
</html>