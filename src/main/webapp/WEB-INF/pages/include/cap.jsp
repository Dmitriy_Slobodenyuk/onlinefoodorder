<%@page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="hello_site_title_bar_wrapper">

    <div id="hello_site_title_bar">
        <%--Логотип сайта--%>
        <div id="site_title">
            <h1><a>
                <img src="/resources/images/main/4.png" alt="Portfolio"/>
                <span>Вкусно и быстро </span>
            </a></h1>
        </div>

        <div id="templatemo_menu">
            <ul>
                <li><a href="${pageContext.request.contextPath}/hello"><span></span>Главная</a></li>
                <li><a href=""><span></span>Aкции</a></li>
                <li><a href="#"><span></span>Контакты</a></li>
                <li><a href="${pageContext.request.contextPath}/admin/login"><span></span>Вход</a></li>
                <li>
                    <form method="post" action="${pageContext.request.contextPath}/findProducts" class="form_find">
                        <input name="what" autocomplete="off" type="text" placeholder="Найти блюдо">
                        <button id="btn_search">
                            <i class="icon_search"></i>
                        </button>
                    </form>
                </li>
            </ul>
        </div>

    </div>

</div>
