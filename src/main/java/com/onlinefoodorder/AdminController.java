package com.onlinefoodorder;

import com.onlinefoodorder.entity.CategoryProduct;
import com.onlinefoodorder.entity.Product;
import com.onlinefoodorder.order.ClientInfo;
import com.onlinefoodorder.order.Order;
import com.onlinefoodorder.service.ServiceOrder;
import com.onlinefoodorder.service.ServiceProduct;
import com.onlinefoodorder.utills.AttributeNameConst;
import com.onlinefoodorder.utills.OrderStatusConst;
import com.onlinefoodorder.utills.ProductConst;
import com.onlinefoodorder.validator.FormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Map;

/**
 * author Slobodenyuk D.
 */

@Controller
@RequestMapping("admin/")
public class AdminController {

    @Autowired
    private ServiceProduct serviceProduct;

    @Autowired
    private ServiceOrder serviceOrder;

    // jump to the page with the registration form
    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String logIn(@RequestParam(value = "error", required = false) String error, ModelMap model) {
        String mainView = "login";
        if (error != null) {
            model.addAttribute("error", "Ошибка ввода : Имя пользователя или пароль набран неверно !");
        }
        return mainView;
    }

    // jump to the page admin panel
    @RequestMapping(value = "main", method = RequestMethod.GET)
    public String returnMain(ModelMap model) {
        String mainView = "admin/admin-main";
        int count = serviceOrder.getOrderDao().getCountOrders(0).get(0);
        model.addAttribute("count", count);
        return mainView;
    }

    //  new product form
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String addProduct(ModelMap model) {
        String view = "admin/add-product";
        return view;
    }

    //  request for removal of the product by its id
    @RequestMapping(value = "deleteProduct/{id}/{name}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable("id") Integer id, @PathVariable("name") String name, ModelMap model) {
        String view = "forward:/admin/listProductDelete/" + name;
        serviceProduct.getProductDao().deleteProduct(id);
        return view;
    }

    @RequestMapping(value = "getInfo/{id}", method = RequestMethod.GET)
    public String returnItemCart(@PathVariable("id") Long key, ModelMap model) {
        Order order = serviceOrder.getOrderDao().getById(key).get(0);
        ClientInfo clientInfo = serviceOrder.getClientInfoDao().getById(order.getId_client()).get(0);
        model.addAttribute("idOrder", order.getId());
        model.addAttribute("name", clientInfo.getName());
        model.addAttribute("phone", clientInfo.getPhone());
        model.addAttribute("mail", clientInfo.getMail());
        model.addAttribute("adress", clientInfo.getStreet() + " " + clientInfo.getHomeNumber() + "/" +
                clientInfo.getOfficeNumber());
        model.addAttribute("price", order.getPrice());
        model.addAttribute("comment", order.getComment());
        Map<Product, Integer> map = serviceProduct.getProductDao().getListProductsOrder(key);
        model.addAttribute("map", serviceProduct.getProductDao().getListProductsOrder(key));

        String cart = "order/order-info";
        return cart;
    }

    @RequestMapping(value = "saveModifiedProduct", method = RequestMethod.POST)
    public String saveModifiedProduct(@RequestParam(value = "productId", required = false) Long id,
                                      @RequestParam(value = "productName", required = false) String name,
                                      String type, String weight,
                                      String price, String photo,
                                      @RequestParam(value = "photoFile") MultipartFile file,
                                      String description,
                                      ModelMap model) {


        if (!FormValidator.checkOnNull(name, weight, price)) {
            String view = "redirect:/admin/listProductEdit/pizza";
            Product newProduct = new Product();
            newProduct.setId(id);
            newProduct.setName(name);
            newProduct.setWeight(Integer.parseInt(weight));
            newProduct.setPrice(Double.parseDouble(price));
            newProduct.setDescription(description);
            CategoryProduct categoryProduct = serviceProduct.getCategoryProductDao().getByName(type).get(0);
            newProduct.setCategory(categoryProduct);
            if (!file.isEmpty()) {
                newProduct.setImage(saveImageProduct(file, type));
            } else {
                newProduct.setImage(photo);
            }
            serviceProduct.getProductDao().saveModified(newProduct);
            return view;
        } else {
            String view = "admin/edit-product-form";
            model.addAttribute("state", "Есть не заполненные поля !");
            return view;
        }

    }

    /**
     * receive the product by id and displays it
     * on the form where the admin can edit them
     * and saves the edited product already
     */
    @RequestMapping(value = "editProduct/{id}", method = RequestMethod.GET)
    public String editProduct(@PathVariable("id") Integer id, ModelMap model) {
        Product product = serviceProduct.getProductDao().getId(id).get(0);
        CategoryProduct categoryProduct = serviceProduct.getCategoryProductDao().getById(product.getCategory().getId()).get(0);

        model.addAttribute("id", product.getId());
        model.addAttribute("name", product.getName());
        model.addAttribute("type", categoryProduct.getName());
        model.addAttribute("weight", product.getWeight());
        model.addAttribute("price", product.getPrice());
        model.addAttribute("photo", product.getImage());
        model.addAttribute("description", product.getDescription());
        String view = "admin/edit-product-form";
        return view;
    }

    private void selectCategoryProduct(String type, ModelMap model) {
        switch (type) {
            case "pizza":
                model.addAttribute(AttributeNameConst.NAME.toString(), "pizza");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.PIZZA.toString()));
                break;
            case "sushi":
                model.addAttribute(AttributeNameConst.NAME.toString(), "sushi");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.SUSHI.toString()));
                break;
            case "fastFood":
                model.addAttribute(AttributeNameConst.NAME.toString(), "fastFood");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.FAST_FOOD.toString()));
                break;
            case "meat":
                model.addAttribute(AttributeNameConst.NAME.toString(), "meat");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.MEAT.toString()));
                break;
            case "homeMenu":
                model.addAttribute(AttributeNameConst.NAME.toString(), "homeMenu");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.HOME_COOKING.toString()));
                break;
            case "baking":
                model.addAttribute(AttributeNameConst.NAME.toString(), "baking");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.BAKING.toString()));
                break;
            case "drink":
                model.addAttribute(AttributeNameConst.NAME.toString(), "drink");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.DRINKS.toString()));
                break;
            case "launch":
                model.addAttribute(AttributeNameConst.NAME.toString(), "launch");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.BUSINESS_LAUNCH.toString()));
                break;
            default:
                model.addAttribute(AttributeNameConst.NAME.toString(), "pizza");
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), serviceProduct.getProductDao().getAll(ProductConst.PIZZA.toString()));
                break;
        }
    }

    /**
     * on page Product Selection to remove
     */
    @RequestMapping(value = "listProductDelete/{productType}", method = RequestMethod.GET)
    public String returnListForDelete(@PathVariable("productType") String type, ModelMap model) {
        String viewName = "admin/delete-product";
        selectCategoryProduct(type, model);
        return viewName;
    }

    /**
     * product selection page for editing
     */
    @RequestMapping(value = "listProductEdit/{productType}", method = RequestMethod.GET)
    public String returnListForEdit(@PathVariable("productType") String type, ModelMap model) {
        String viewName = "admin/edit-product";
        selectCategoryProduct(type, model);
        return viewName;
    }


    /**
     * take the form of data
     * and maintain a new product
     */
    @RequestMapping(value = "saveProduct", method = RequestMethod.POST)
    public String saveProduct(@RequestParam(value = "productName", required = false) String name,
                              String type, String weight,
                              String price,
                              @RequestParam(value = "photoFile") MultipartFile file,
                              String description,
                              ModelMap model) {

        String view = "admin/add-product";
        if (!FormValidator.checkOnNull(name, weight, price)) {
            Product newProduct = new Product();
            newProduct.setName(name);
            newProduct.setWeight(Integer.parseInt(weight));
            newProduct.setPrice(Double.parseDouble(price));
            newProduct.setDescription(description);
            CategoryProduct category = serviceProduct.getCategoryProductDao().getByName(type).get(0);
            newProduct.setCategory(category);
            if (!file.isEmpty()) {
                newProduct.setImage(saveImageProduct(file, type));
            }
            serviceProduct.getProductDao().save(newProduct);
            model.addAttribute("state", "Добавлено !");
            return view;
        } else {
            model.addAttribute("state", "Есть не заполненные поля !");
            return view;
        }
    }

    /**
     * Saves the selected product photo of admin
     * the file system at the right place for us
     */
    private String saveImageProduct(MultipartFile file, String typeProduct) {
        String dir = null;
        String path = null;

        switch (typeProduct) {
            case "Пицца":
                dir = "pizza";
                path = "/resources/images/pizza/" + file.getOriginalFilename();
                break;
            case "Суши":
                dir = "sushi";
                path = "/resources/images/sushi/" + file.getOriginalFilename();
                break;
            case "фастфуд":
                dir = "fastfood";
                path = "/resources/images/fastfood/" + file.getOriginalFilename();
                break;
            case "Мясное блюдо":
                dir = "meet";
                path = "/resources/images/meet/" + file.getOriginalFilename();
                break;
            case "Домашнее меню":
                dir = "homemenu";
                path = "/resources/images/homemenu/" + file.getOriginalFilename();
                break;
            case "Выпечка":
                dir = "baking";
                path = "/resources/images/baking/" + file.getOriginalFilename();
                break;
            case "Напитки":
                dir = "drinks";
                path = "/resources/images/drinks/" + file.getOriginalFilename();
                break;
            case "Бизнес-ланч":
                dir = "lanch";
                path = "/resources/images/lanch/" + file.getOriginalFilename();
                break;
            default:
                dir = "lanch";
                path = "/resources/images/lanch/" + file.getOriginalFilename();
                break;
        }
        InputStream inputStream = null;
        OutputStream outputStream = null;


        String fileName = file.getOriginalFilename();

        try {
            inputStream = file.getInputStream();

            File newFile = new File("D:\\Development\\Java\\Диплом\\OnlineFoodOrder\\src\\main\\webapp\\resources\\images\\"
                    + dir + "\\" + fileName);

            if (!newFile.exists()) {
                newFile.createNewFile();
            }
            outputStream = new FileOutputStream(newFile);
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    /**
     * to a page that displays a list of orders
     */
    @RequestMapping(value = "orderNotAccepts/{typeOrder}", method = RequestMethod.GET)
    public String orderNotAccepts(@PathVariable("typeOrder") String type, ModelMap model) {
        String viewName = "order/listOrders";
        switch (type) {
            case "notAccepted":
                int idNotAccepted = 0;
                model.addAttribute(AttributeNameConst.TYPE_LIST.toString(), "notAccepted");
                model.addAttribute(AttributeNameConst.ORDER_LIST.toString(), serviceOrder.getOrderDao().getAllStatus(idNotAccepted));
                break;
            case "accepted":
                int idAccepted = 1;
                model.addAttribute(AttributeNameConst.TYPE_LIST.toString(), "accepted");
                model.addAttribute(AttributeNameConst.ORDER_LIST.toString(), serviceOrder.getOrderDao().getAllStatus(idAccepted));
                break;
            case "delivered":
                int idDelivered = 2;
                model.addAttribute(AttributeNameConst.TYPE_LIST.toString(), "delivered");
                model.addAttribute(AttributeNameConst.ORDER_LIST.toString(), serviceOrder.getOrderDao().getAllStatus(idDelivered));
                break;
            default:
                model.addAttribute(AttributeNameConst.ORDER_LIST.toString(), serviceOrder.getOrderDao().getAll());
                break;
        }
        return viewName;
    }

    /**
     * delete order by id
     */
    @RequestMapping(value = "deleteOrderAdmin/{id}/{typeList}", method = RequestMethod.GET)
    public String deleteOrder(@PathVariable("id") Long id,
                              @PathVariable("typeList") String typeList,
                              ModelMap model) {
        String viewName = "forward:/admin/orderNotAccepts/" + typeList;
        serviceOrder.getOrderDao().safeUpdate();
        serviceOrder.getOrderDao().deleteListProductsByOrder(id);
        serviceOrder.getOrderDao().deleteOrder(id);
        return viewName;
    }

    /**
     * to the page with the right type of orders
     */
    @RequestMapping(value = "orderChangeAccepts/{typeOrder}/{typeList}/{orderId}", method = RequestMethod.GET)
    public String orderChangeAccepts(@PathVariable("typeOrder") String type, @PathVariable("typeList") String typeList,
                                     @PathVariable("orderId") Long idOrder,
                                     ModelMap model) {
        String viewName = "forward:/admin/orderNotAccepts/" + typeList;

        switch (type) {
            case "notAccepted":
                int idNotAccepted = OrderStatusConst.ORDER_NOT_ACCEPTED.getCode();
                serviceOrder.getOrderDao().changeStatusOrder(idNotAccepted, idOrder);
                break;
            case "accepted":
                int idAccepted = OrderStatusConst.ORDER_ACCEPTED.getCode();
                serviceOrder.getOrderDao().changeStatusOrder(idAccepted, idOrder);
                break;
            case "delivered":
                int idDelivered = OrderStatusConst.ORDER_FINISHED.getCode();
                serviceOrder.getOrderDao().changeStatusOrder(idDelivered, idOrder);
                break;
            default:
                viewName = "admin/admin-main";
                break;
        }
        return viewName;
    }
}
