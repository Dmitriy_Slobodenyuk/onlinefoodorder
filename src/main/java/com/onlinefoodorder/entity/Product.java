package com.onlinefoodorder.entity;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Objects;

/**
 * author Slobodenyuk D.
 */
public class Product implements Serializable{
    private long id;

    /**
     * Annotations for use with form Validation add product
     * admin's part Projects
     */
    @NotEmpty
    private String name;

    @NotEmpty
    private int weight;

    @NotEmpty
    private double price;
    private String description;
    // default image
    private String image = "/resources/images/no-photo.png";
    private CategoryProduct category;

    public Product() {
    }

    public Product(String name, int weight, double price, String description, String image) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.description = description;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        if (!image.equals("")) this.image = image;
    }

    public CategoryProduct getCategory() {
        return category;
    }

    public void setCategory(CategoryProduct category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final Product other = (Product) o;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public String toString() {
        return getName() + " " + getPrice();
    }
}
