package com.onlinefoodorder.entity;

import java.io.Serializable;

/**
 * author Slobodenyuk D.
 */
public class CategoryProduct implements Serializable {
    private long id;
    private String name;

    public CategoryProduct() {
    }

    public CategoryProduct(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
