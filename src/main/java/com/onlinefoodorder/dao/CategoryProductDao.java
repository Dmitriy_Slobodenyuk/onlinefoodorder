package com.onlinefoodorder.dao;

import com.onlinefoodorder.entity.CategoryProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * author Slobodenyuk D.
 */
@Repository("categoryProductDao")
public class CategoryProductDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    public synchronized List<CategoryProduct> getByName(String name) {
        String query = "SELECT * FROM category where categoryName=?";
        return jdbcTemplate.query(query, new Object[]{name}, new CategoryProductCreator());
    }

    public synchronized List<CategoryProduct> getById(long id) {
        String query = "SELECT * FROM category where id_category=?";
        return jdbcTemplate.query(query, new Object[]{id}, new CategoryProductCreator());
    }

    @PostConstruct
    private void init() {
        jdbcInsert.withTableName("category").usingGeneratedKeyColumns("id_category");
    }
}

class CategoryProductCreator implements RowMapper<CategoryProduct> {
    @Override
    public CategoryProduct mapRow(ResultSet rs, int rowNum) throws SQLException {
        CategoryProduct categoryProduct = new CategoryProduct();
        categoryProduct.setId(rs.getLong("id_category"));
        categoryProduct.setName(rs.getString("categoryName"));
        return categoryProduct;
    }

}
