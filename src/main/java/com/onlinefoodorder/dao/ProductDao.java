package com.onlinefoodorder.dao;

import com.onlinefoodorder.entity.CategoryProduct;
import com.onlinefoodorder.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author Slobodenyuk D.
 */
@Repository("productDao")
public class ProductDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    public synchronized List<Product> getAll(String nameCategory) {
        String query = "SELECT * FROM products,category WHERE category_id=id_category and categoryName LIKE ?";
        return jdbcTemplate.query(query, new Object[]{nameCategory}, new ProductCreator());
    }

    /**
     * retrieve a list of products for a specific order
     */
    public synchronized Map<Product, Integer> getListProductsOrder(long idOrder) {
        String query = "SELECT id_product,productName,weight,products.price,count FROM order_info,products,order_product WHERE " +
                "order_info.order_id=order_product.order_id and product_id=id_product and order_info.order_id=?;";

        Map map = (Map) jdbcTemplate.query(query, new Object[]{idOrder},
                new ResultSetExtractor() {
                    public Object extractData(ResultSet rs) throws SQLException {
                        Map map = new HashMap();
                        while (rs.next()) {
                            Product product = new Product();
                            product.setId(rs.getLong("id_product"));
                            product.setName(rs.getString("productName"));
                            product.setWeight(rs.getInt("weight"));
                            product.setPrice(rs.getDouble("price"));
                            Integer count = rs.getInt("count");
                            map.put(product, count);
                        }
                        return map;
                       }
                });
        return map;
    }

    public synchronized List<Product> getId(int id) {
        String query = "SELECT * FROM products,category WHERE id_product=?" + " and category_id=id_category";
        return jdbcTemplate.query(query, new Object[]{id}, new ProductCreator());
    }

    public synchronized List<Product> findProducts(String find) {
        String query = "SELECT * FROM products,category WHERE category_id=id_category and productName LIKE ?";
        String what = "%" + find + "%";
        return jdbcTemplate.query(query, new Object[]{what}, new ProductCreator());
    }

    public void deleteProduct(int id) {
        String deleteStatement = "DELETE FROM products WHERE id_product=?";
        jdbcTemplate.update(deleteStatement, id);
    }

    public void saveModified(Product product) {
        String alter = "UPDATE products SET productName = ?, weight = ?, price = ?, description = ?, image = ?, category_id = ? " +
                "WHERE id_product = ?";
        jdbcTemplate.update(alter, new Object[]{product.getName(),
                product.getWeight(), product.getPrice(),
                product.getDescription(), product.getImage(), product.getCategory().getId(), product.getId()});
    }

    public void save(Product product) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("productName", product.getName());
        params.put("weight", product.getWeight());
        params.put("price", product.getPrice());
        params.put("description", product.getDescription());
        params.put("image", product.getImage());
        params.put("category_id", product.getCategory().getId());
        Number key = jdbcInsert.executeAndReturnKey(params);

        product.setId((Long) key);
    }

    @PostConstruct
    private void init() {
        jdbcInsert.withTableName("products").usingGeneratedKeyColumns("id_product");
    }
}

class ProductCreator implements RowMapper<Product> {
    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product();
        CategoryProduct categoryProduct = new CategoryProduct();
        product.setId(rs.getLong("id_product"));
        product.setName(rs.getString("productName"));
        product.setWeight(rs.getInt("weight"));
        product.setPrice(rs.getDouble("price"));
        product.setDescription(rs.getString("description"));
        product.setImage(rs.getString("image"));
        categoryProduct.setId(rs.getLong("category_id"));
        product.setCategory(categoryProduct);
        return product;
    }

}
