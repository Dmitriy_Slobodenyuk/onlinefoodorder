package com.onlinefoodorder.cart;

import com.onlinefoodorder.entity.Product;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;


/**
 * Shopping cart
 * contains a list of selected customer products
 * and their number
 * author Slobodenyuk D.
 */

@Component
@Scope("session")
public class Cart {
    /**
     * Container for storing a list of products and their quantities
     */
    private Map<Product, Integer> contents = new HashMap<Product, Integer>();

    /**
     * Remove from cart product
     */
    public void removeItems(Product product) {
        contents.remove(product);
    }

    /**
     * add to cart product
     */
    public void addProduct(Product product) {
        int count = 1;
        product.hashCode();
        if (isContains(product)) {
            Integer currentQuantity = contents.get(product);
            count += currentQuantity.intValue();
        }
        contents.put(product, new Integer(count));
    }

    /**
     * Check the availability of the product in the cart
     */
    public boolean isContains(Product p) {
        if (contents.containsKey(p)) {
            return true;
        } else {
            return false;
        }
    }

    public List<Product> getListContents() {
        List<Product> list = new ArrayList<Product>();
        for (Iterator<Product> I = contents.keySet().iterator(); I.hasNext(); ) {
            list.add(I.next());
        }
        return list;
    }

    public Map<Product, Integer> getContents() {
        return contents;
    }

    public void setContents(Map<Product, Integer> contents) {
        this.contents = contents;
    }

    public String getCartTotal() {
        double total = 0;
        for (Iterator<Product> I = contents.keySet().iterator(); I.hasNext(); ) {
            Product item = I.next();
            int itemQuantity = contents.get(item).intValue();
            total += (item.getPrice() * itemQuantity);
        }
        return new BigDecimal(total).setScale(2, RoundingMode.CEILING).toString();
    }
}
