package com.onlinefoodorder;

import com.onlinefoodorder.cart.Cart;
import com.onlinefoodorder.entity.Product;
import com.onlinefoodorder.order.ClientInfo;
import com.onlinefoodorder.order.Order;
import com.onlinefoodorder.order.dao.ClientInfoDao;
import com.onlinefoodorder.order.dao.OrderDao;
import com.onlinefoodorder.service.ServiceOrder;
import com.onlinefoodorder.service.ServiceProduct;
import com.onlinefoodorder.utills.AttributeNameConst;
import com.onlinefoodorder.utills.ProductConst;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/")
public class HelloController implements ApplicationContextAware {
    @Autowired
    @Qualifier("clientValidator")
    Validator validator;

    @Autowired
    private ServiceProduct service;

    @Autowired
    private Cart cart;

    @Autowired
    private ServiceOrder serviceOrder;

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        String helloView = "hello";
        return helloView;
    }

    /**
     * to get the product on its id
     */
    @RequestMapping(value = "listProductById/{idProduct}", method = RequestMethod.GET)
    public String returnListProductById(@PathVariable("idProduct") Integer id, ModelMap model) {
        String cartView = "cart/cart";
        List<Product> list = service.getProductDao().getId(id);
        cart.addProduct(list.get(0));
        model.addAttribute(AttributeNameConst.LIST_ORDER.toString(), cart.getListContents());
        return cartView;
    }

    /**
     * get a list of products in category
     */
    @RequestMapping(value = "listProduct/{productType}", method = RequestMethod.GET)
    public String returnList(@PathVariable("productType") String type, ModelMap model) {
        String viewName = AttributeNameConst.LIST_PRODUCT.toString();
        switch (type) {
            case "pizza":
                model.addAttribute(AttributeNameConst.NAME.toString(), ProductConst.PIZZA.toString());
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), service.getProductDao().getAll(ProductConst.PIZZA.toString()));
                break;
            case "sushi":
                model.addAttribute(AttributeNameConst.NAME.toString(), ProductConst.SUSHI.toString());
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), service.getProductDao().getAll(ProductConst.SUSHI.toString()));
                break;
            case "fastFood":
                model.addAttribute(AttributeNameConst.NAME.toString(), ProductConst.FAST_FOOD.toString());
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), service.getProductDao().getAll(ProductConst.FAST_FOOD.toString()));
                break;
            case "meat":
                model.addAttribute(AttributeNameConst.NAME.toString(), ProductConst.MEAT.toString());
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), service.getProductDao().getAll(ProductConst.MEAT.toString()));
                break;
            case "homeMenu":
                model.addAttribute(AttributeNameConst.NAME.toString(), ProductConst.HOME_COOKING.toString());
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), service.getProductDao().getAll(ProductConst.HOME_COOKING.toString()));
                break;
            case "baking":
                model.addAttribute(AttributeNameConst.NAME.toString(), ProductConst.BAKING.toString());
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), service.getProductDao().getAll(ProductConst.BAKING.toString()));
                break;
            case "drink":
                model.addAttribute(AttributeNameConst.NAME.toString(), ProductConst.DRINKS.toString());
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), service.getProductDao().getAll(ProductConst.DRINKS.toString()));
                break;
            case "launch":
                model.addAttribute(AttributeNameConst.NAME.toString(), ProductConst.BUSINESS_LAUNCH.toString());
                model.addAttribute(AttributeNameConst.LIST_PRODUCT.toString(), service.getProductDao().getAll(ProductConst.BUSINESS_LAUNCH.toString()));
                break;
            default:
                viewName = "hello";
                break;
        }
        return viewName;
    }

    /**
     * go to the main page of the site
     */
    @RequestMapping(value = "hello", method = RequestMethod.GET)
    public String returnHello(ModelMap model) {
        String helloView = "hello";
        return helloView;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        // context = applicationContext;
    }

    /**
     * update the shopping cart
     * Here we check if the basket is not empty
     * then derive its contents
     */
    @RequestMapping(value = "updateCartMethod", method = RequestMethod.GET)
    public String update(ModelMap model) {
        if (cart.getContents().size() == 0) {
            String empty = "cart/cart_empty";
            return empty;
        } else {
            String item = "cart/cart_item";
            model.addAttribute("cartList", cart.getContents());
            model.addAttribute("totalPrice", cart.getCartTotal());
            return item;
        }
    }

    /**
     * add the selected product in the shopping cart
     */
    @RequestMapping(value = "listItemCart/{id}", method = RequestMethod.GET)
    public String returnItemCart(@PathVariable("id") Integer key, ModelMap model) {
        Product product = service.getProductDao().getId(key).get(0);
        cart.addProduct(product);
        model.addAttribute("cartList", cart.getContents());
        model.addAttribute("totalPrice", cart.getCartTotal());
        String cart = "cart/cart_item";
        return cart;
    }

    /**
     * removing the product from the basket
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    public String gelItemCart(@PathVariable("id") Integer key, ModelMap model) {

        Product product = service.getProductDao().getId(key).get(0);
        cart.removeItems(product);
        model.addAttribute("cartList", cart.getContents());
        model.addAttribute("totalPrice", cart.getCartTotal());
        String cart = "cart/cart_item";

        return cart;
    }

    /**
     * go to the order form
     */
    @RequestMapping(value = "order", method = RequestMethod.GET)
    public String makeOrder(ModelMap model) {
        String order = "order/makeOrder";
        model.addAttribute("total", cart.getCartTotal());
        model.addAttribute("clientInfo", new ClientInfo());
        return order;
    }

    /**
     * Search product by name
     */
    @RequestMapping(value = "findProducts", method = RequestMethod.POST)
    public String findProducts(@RequestParam(value = "what", required = false) String what, ModelMap model) {

        List<Product> list = service.getProductDao().findProducts(what);
        String viewName = "listProduct";
        if (list.isEmpty()) {
            model.addAttribute("message", "Ничего не найдено , попробуйте еще раз !");
            viewName = "error/errorSearch";
        } else {
            model.addAttribute("listProduct", list);
            viewName = AttributeNameConst.LIST_PRODUCT.toString();
        }
        return viewName;
    }

    /**
     * take the data from the form and store the new order
     * immediately check the correctness of the fields
     */
    @RequestMapping(value = "saveOrder", method = RequestMethod.POST)
    public String makeHello(@RequestParam(value = "comment", required = false) String comment,
                            @Valid ClientInfo clientInfo, BindingResult result, ModelMap model) {
        String orderView = "order/makeOrder";

        if (!result.hasErrors()) {
            serviceOrder.getClientInfoDao().save(clientInfo);
            Order order = new Order(clientInfo.getId(), Double.parseDouble(cart.getCartTotal()), comment,
                    0, new Date());
            serviceOrder.getOrderDao().save(order);
            order.setProducts(cart.getContents());
            serviceOrder.getOrderDao().saveListProducts(order.getProducts(), order.getId());
            //Logger LOGGER = Logger.getLogger("InfoLogging");
            //LOGGER.info(order.getDateOrder().toString());
            model.addAttribute("total", cart.getCartTotal());
            model.addAttribute("message", "Приятного аппетита !");
            return orderView;
        } else {
            model.addAttribute("total", cart.getCartTotal());
            return orderView;
        }
    }
}