package com.onlinefoodorder.service;


import com.onlinefoodorder.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("serviceProduct")
public class ServiceProduct {
    @Autowired
    private ProductDao productDao;

    @Autowired
    private CategoryProductDao categoryProductDao;

    public ProductDao getProductDao() {
        return productDao;
    }

    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    public CategoryProductDao getCategoryProductDao() {
        return categoryProductDao;
    }

    public void setCategoryProductDao(CategoryProductDao categoryProductDao) {
        this.categoryProductDao = categoryProductDao;
    }
}
