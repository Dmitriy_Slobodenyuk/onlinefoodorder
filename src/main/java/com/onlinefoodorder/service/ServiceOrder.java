package com.onlinefoodorder.service;

import com.onlinefoodorder.order.dao.ClientInfoDao;
import com.onlinefoodorder.order.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("serviceOrder")
public class ServiceOrder {
    @Autowired
    private OrderDao orderDao;

    @Autowired
    private ClientInfoDao clientInfoDao;

    public OrderDao getOrderDao() {
        return orderDao;
    }

    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public ClientInfoDao getClientInfoDao() {
        return clientInfoDao;
    }

    public void setClientInfoDao(ClientInfoDao clientInfoDao) {
        this.clientInfoDao = clientInfoDao;
    }
}
