package com.onlinefoodorder.validator;

import com.onlinefoodorder.order.ClientInfo;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Service(value = "clientValidator")
public class ClientInfoFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> paramClass) {
        return ClientInfo.class.equals(paramClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "street", "street.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mail", "mail.required");
    }
}
