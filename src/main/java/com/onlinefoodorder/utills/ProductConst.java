package com.onlinefoodorder.utills;

public enum ProductConst {

    PIZZA("Пицца"),
    SUSHI("Суши"),
    FAST_FOOD("фастфуд"),
    MEAT("Мясное блюдо"),
    HOME_COOKING("Домашнее меню"),
    BAKING("Выпечка"),
    DRINKS("Напитки"),
    BUSINESS_LAUNCH("Бизнес-Ланч")
    ;

    private final String nameProduct;

    ProductConst(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String toString() {
        return nameProduct;
    }

}
