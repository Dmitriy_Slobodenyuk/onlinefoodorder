package com.onlinefoodorder.utills;

public enum OrderStatusConst {
    ORDER_NOT_ACCEPTED(0),
    ORDER_ACCEPTED(1),
    ORDER_FINISHED(2),
    ORDER_FAILURE(3);

    private final int code;

    OrderStatusConst(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
