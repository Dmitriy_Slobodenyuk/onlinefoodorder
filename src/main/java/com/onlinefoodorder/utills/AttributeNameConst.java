package com.onlinefoodorder.utills;

public enum AttributeNameConst {
    NAME("name"),
    LIST_PRODUCT("listProduct"),
    LIST_ORDER("listOrder"),
    ORDER_LIST("orderList"),
    TYPE_LIST("typeList")
    ;

    private final String name;

    AttributeNameConst(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
