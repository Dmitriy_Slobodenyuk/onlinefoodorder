package com.onlinefoodorder.order;

import com.onlinefoodorder.validator.Phone;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import java.io.Serializable;

/**
 * will contain information about the customer
 */
public class ClientInfo implements Serializable {
    private long id;

    @NotEmpty
    private String name;

    @Phone
    private String phone;

    @Email
    @NotEmpty
    private String mail;

    @NotEmpty
    private String street;

    @NotEmpty
    private String homeNumber;

    @NotEmpty
    private String officeNumber;

    public ClientInfo() {

    }

    public ClientInfo(String name, String phone, String mail, String street, String homeNumber, String officeNumber) {
        this.name = name;
        this.phone = phone;
        this.mail = mail;
        this.street = street;
        this.homeNumber = homeNumber;
        this.officeNumber = officeNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }
}
