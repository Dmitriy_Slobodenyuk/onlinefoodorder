package com.onlinefoodorder.order.dao;

import com.onlinefoodorder.entity.CategoryProduct;
import com.onlinefoodorder.entity.Product;
import com.onlinefoodorder.order.ClientInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository("clientInfoDao")
public class ClientInfoDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    public List<ClientInfo> getAll() {
        String query = "SELECT * FROM client";
        return jdbcTemplate.query(query, new Object[]{}, new ClientInfoCreator());
    }

    public List<ClientInfo> getById(Long id) {
        String query = "SELECT * FROM client where client_id=?";
        return jdbcTemplate.query(query, new Object[]{id}, new ClientInfoCreator());
    }

    public List<ClientInfo> getByName(String name) {
        String query = "SELECT * FROM client where clientName=?";
        return jdbcTemplate.query(query, new Object[]{name}, new ClientInfoCreator());
    }

    public void save(ClientInfo clientInfo) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("clientName", clientInfo.getName());
        params.put("phone", clientInfo.getPhone());
        params.put("mail", clientInfo.getMail());
        params.put("street", clientInfo.getStreet());
        params.put("home", clientInfo.getHomeNumber());
        params.put("office", clientInfo.getOfficeNumber());
        Number key = jdbcInsert.executeAndReturnKey(params);
        clientInfo.setId((Long) key);
    }

    @PostConstruct
    private void init() {
        jdbcInsert.withTableName("client").usingGeneratedKeyColumns("client_id");
    }
}

class ClientInfoCreator implements RowMapper<ClientInfo> {
    @Override
    public ClientInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setId(rs.getLong("client_id"));
        clientInfo.setName(rs.getString("clientName"));
        clientInfo.setPhone(rs.getString("phone"));
        clientInfo.setMail(rs.getString("mail"));
        clientInfo.setStreet(rs.getString("street"));
        clientInfo.setHomeNumber(rs.getString("home"));
        clientInfo.setOfficeNumber(rs.getString("office"));
        return clientInfo;
    }

}
