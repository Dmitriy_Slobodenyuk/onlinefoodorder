package com.onlinefoodorder.order.dao;

import com.onlinefoodorder.entity.Product;
import com.onlinefoodorder.order.ClientInfo;
import com.onlinefoodorder.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.*;

@Repository("orderDao")
public class OrderDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    public List<Order> getAll() {
        String query = "SELECT * FROM order_info, client where id_client = client_id";
        return jdbcTemplate.query(query, new Object[]{}, new OrderCreator());
    }

    public List<Integer> getCountOrders(int idStatus) {
        String query = "SELECT COUNT(*) FROM order_info where status=?";
        return jdbcTemplate.query(query, new Object[]{idStatus}, new CountCreator());
    }

    public List<Order> getAllStatus(int idStatus) {
        String query = "SELECT * FROM order_info, client where id_client = client_id and status=?";
        return jdbcTemplate.query(query, new Object[]{idStatus}, new OrderCreator());
    }

    public List<Order> getById(Long id) {
        String query = "SELECT * FROM order_info where order_id=?";
        return jdbcTemplate.query(query, new Object[]{id}, new OrderCreator());
    }

    public void changeStatusOrder(int status, Long idOrder) {
        String alter = "UPDATE order_info SET status = ? WHERE order_id = ?";
        jdbcTemplate.update(alter, new Object[]{status, idOrder});
    }

    public void deleteOrder(long idOrder) {
        String deleteStatement = "DELETE FROM order_info WHERE order_id=?";
        jdbcTemplate.update(deleteStatement, idOrder);
    }

    public void safeUpdate() {
        String deleteStatement = "SET SQL_SAFE_UPDATES=0";
        jdbcTemplate.update(deleteStatement);
    }

    public void deleteListProductsByOrder(long idOrder) {
        String deleteStatement = "DELETE FROM order_product WHERE order_id=?";
        jdbcTemplate.update(deleteStatement, idOrder);
    }

    public void save(Order order) {
        Map params = new HashMap();
        params.put("id_client", order.getId_client());
        params.put("price", order.getPrice());
        params.put("comment", order.getComment());
        params.put("status", order.getStatus());
        params.put("data", order.getDateOrder());
        Number key = jdbcInsert.executeAndReturnKey(params);
        order.setId((Long) key);
    }

    @PostConstruct
    private void init() {
        jdbcInsert.withTableName("order_info").usingGeneratedKeyColumns("order_id");
    }


    public void saveListProducts(Map<Product, Integer> map, Long idOrder) {
        String sql = "INSERT INTO "
                + "order_product "
                + "(order_id,product_id,count) "
                + "VALUES " + "(?,?,?)";
        for (Iterator<Product> i = map.keySet().iterator(); i.hasNext(); ) {
            Product item = i.next();
            int itemQuantity = map.get(item).intValue();
            jdbcTemplate.update(sql, new Object[]{idOrder,
                    item.getId(), itemQuantity
            });

        }
    }
}

class CountCreator implements RowMapper<Integer> {
    @Override
    public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer count = 0;
        count = rs.getInt(1);
        return count;
    }
}

class OrderCreator implements RowMapper<Order> {
    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
        Order order = new Order();
        order.setId(rs.getLong("order_id"));
        order.setId_client(rs.getLong("id_client"));
        order.setPrice(rs.getDouble("price"));
        order.setComment(rs.getString("comment"));
        order.setDateOrder(rs.getTimestamp("data"));
        order.setStatus(rs.getInt("status"));
        return order;
    }
}
