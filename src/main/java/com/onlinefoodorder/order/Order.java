package com.onlinefoodorder.order;

import com.onlinefoodorder.entity.Product;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * author Slobodenyuk D.
 */
public class Order implements Serializable {
    private long id;
    private long id_client;
    private double price;
    private String comment;
    private int status;
    private String date;
    private Date dateOrder;
    private Map<Product, Integer> products;

    public Order() {

    }

    public Order(long idClient, double price, String comment, int status, Date date) {
        this.id_client = idClient;
        this.price = price;
        this.comment = comment;
        this.status = status;
        this.dateOrder = date;
    }

    public Order(long idClient, double price, int status, Date date) {
        this.id_client = idClient;
        this.price = price;
        this.status = status;
        this.dateOrder = date;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getId_client() {
        return id_client;
    }

    public void setId_client(long id_client) {
        this.id_client = id_client;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(Date dateOrder) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        DateFormat dateFormat = DateFormat.getTimeInstance(DateFormat.FULL);
        String d = dateFormat.format(dateOrder);
        this.dateOrder = dateOrder;
    }

    public Map<Product, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<Product, Integer> products) {
        this.products = products;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
